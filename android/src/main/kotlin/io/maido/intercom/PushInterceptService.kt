package io.maido.intercom;
import com.google.firebase.messaging.*;
import io.intercom.android.sdk.push.IntercomPushClient;
import android.util.Log;
import java.util.Map;
import io.flutter.plugins.firebasemessaging.*;

class PushInterceptService : FlutterFirebaseMessagingService() {
  private val TAG = "PushInterceptService"
  private val intercomPushClient = IntercomPushClient()

  override
  fun onMessageReceived(remoteMessage: RemoteMessage) {
    super.onMessageReceived(remoteMessage)

    val message = remoteMessage.data

    if (intercomPushClient.isIntercomPush(message)) {
      Log.d(TAG, "Intercom message received")
      intercomPushClient.handlePush(application, message);
    } else {
      Log.d(TAG, "Fuck you, Intercom")
      super.onMessageReceived(remoteMessage)
    }
  }
}